#!/bin/bash

# sofern kein Schaltjahr beteiligt ist, ist unser Programm
# wahrscheinlich korrekt.

# http://mywiki.wooledge.org/BashGuide/Practices#Debugging
# Stepping code
# trap '(read -p "[$BASH_SOURCE:$LINENO] $BASH_COMMAND?")' DEBUG
# export PS4='+$BASH_SOURCE:$LINENO:$FUNCNAME: '

# export PS4='+\n\n\n[$BASH_SOURCE:$LINENO] $FUNCNAME: \n$BASH_COMMAND?\n'

export PS4='\n\n\n[$BASH_SOURCE:$LINENO] $FUNCNAME: \n'

# set -x

. ./globalroutines.sh

# Anzahl der Tage zwischen zwei Daten

# Eingabe: Zwei Daten
# Ausgabe: Die Anzahl der Tage, die zwischen den Daten liegen
# hierbei wird das spätere Datum mitgezählt
# und das Anfangsdatum nicht (klassische Differenz)

validatedates () {

    local date1
    local date2
    local valid1
    local valid2

    if [ 2 -ne "$#" ]
    then
        echo WTFvalidate
        echo "needs date1, date2"
        exit 1
    fi

    date1="$1"
    date2="$2"

    valid1=$(./datevalidation.sh "$date1" "true")
    # echo "$valid1"
    case "$valid1" in
        "noleap")
            verboseoutput "ok"
            ;;
        "false")
            verboseoutput "first date invalid"
            exit 3
            ;;
        "leap")
            verboseoutput "first date in leap year"
            ;;
        *)
            debugoutput "BOOM"
    esac

    valid2=$(./datevalidation.sh "$date2" "true")
    # echo "$valid2"
    case "$valid2" in
        "noleap")
            verboseoutput "ok"
            ;;
        "false")
            verboseoutput "second date invalid"
            exit 3
            ;;
        "leap")
            verboseoutput "second date in leap year"
            ;;
        *)
            debugoutput "BOOM"
    esac




}




## Hauptprogramm

# trap '(read -p "[$BASH_SOURCE:$LINENO] $BASH_COMMAND?")' DEBUG

if [ 2 -ne "$#" ]
then
    echo "needs date1, date2"
    exit 1
fi

datum1="$1"
datum2="$2"

validatedates "$1" "$2"

if isEarlierThan "$datum1" "$datum2"
then
    earlierdate="$datum1"
    laterdate="$datum2"
else
    earlierdate="$datum2"
    laterdate="$datum1"
fi

result=0
debugoutput "tempresult=0"

# Zuerst die vollen Jahre abarbeiten

y1=$(year "$earlierdate")
# debugoutput "earlier year is $y1"
y2=$(year "$laterdate")
# debugoutput "later year is $y2"

for ((i=y1+1; i<y2; i++))
do
    if isLeapYear "$i"
    then
        result=$((result+366))
        debugoutput "tempresult=$result"'   '"($i is a leap year)"
    else
        result=$((result+365))
        debugoutput "tempresult=$result"'   '"($i is not a leap year)"
    fi
done

# Jetzt die vollen Monate im ersten Jahr abarbeiten

m1=$(month "$earlierdate")
# debugoutput "Monat im früheren Jahr ist $m1"
if [ "$y1" -eq "$y2" ]
then
    # m2 ist nicht notwendigerweise ein voller Monat
    m2=$(month "$laterdate")
else
    m2=13 # Dezember (12. Monat) ist auch voller Monat
fi
# debugoutput "spätester Monat im früheren Jahr ist $m2"

for ((i=m1+1; i<m2; i++))
do    
    result=$((result+monthlengths[i]))
    # Achtung Schaltjahr!
    if isLeapYear "$(year "$earlierdate")"; then
        if [ "$i" -eq "2" ]
        then
            ((result++))
        fi
    fi
    debugoutput "tempresult=$result"'   '"($i/$y1)"
done

# Jetzt die vollen Monate im lezten Jahr abarbeiten

donothing=false
m1=$(month "$earlierdate")
m2=$(month "$laterdate")
if [ "$y1" -eq "$y2" ]
then
    donothing=true # sonst würden tage doppelt gezählt
else
    for ((i=1; i<m2; i++))
    do
        result=$((result+monthlengths[i]))
        # Achtung Schaltjahr!
        if isLeapYear "$(year "$earlierdate")"; then
            if [ "$i" -eq "2" ]
            then
                ((result++))
            fi
        fi
        debugoutput "tempresult=$result"'   '"($i/$y2)"
    done
fi





# Jetzt die Tage im ersten Monat abarbeiten

if [ "$y1" -eq "$y2" ] && [ "$m1" -eq "$m2" ]
then
    result=$((result+$(day "$laterdate")-$(day "$earlierdate")))
else
    result=$((result+monthlengths[m1]-$(day "$earlierdate")))
fi
debugoutput "tempresult=$result"'   '"($m1/$y1)"


# Jetzt die Tage im letzten Monat abarbeiten

if [ "$y1" -eq "$y2" ] && [ "$m1" -eq "$m2" ]
then
    true # do nothing
else
    result=$((result+$(day "$laterdate")))
fi
debugoutput "tempresult=$result"'   '"($m2/$y2)"

echo "$result"

epochearlierdate=$(date -ud "$earlierdate"'T12:00' +%s)
epochlaterdate=$(date -ud "$laterdate"'T12:00' +%s)

echo "($epochlaterdate-$epochearlierdate)/86400" | bc
