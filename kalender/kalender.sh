#!/bin/bash

. ./globalroutines.sh

usage() {

    debugoutput "Usage: $0 [-v] <date>
          where <date> is a valid date in ISO 8601 format (YYYY-MM-DD)
          -v turns on verbose output (yet to be defined)"
}

readdate() {

    datum="$1"
    regex='^[[:digit:]]{4}-[[:digit:]]{2}-[[:digit:]]{2}$'
    if [[ ! "$datum" =~ $regex ]] # no quoting....
    then
        debugoutput "${datum} is not a valid date"
        exit 5
    fi
}

parsedate() {

    year=${datum:0:4}
    month=${datum:5:2}
    month=$((10#$month))
    day=${datum:8:2}
    day=$((10#$day))
}

# if [ 1 -ne "$#" ]
# then
#     usage
#     exit 1
# fi

# http://wiki.bash-hackers.org/howto/getopts_tutorial
verbose=false
while getopts ":v" opt; do
  case "$opt" in
    v)
      echo "-v was triggered!" >&2
      verbose=true
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      ;;
  esac
done

# echo "verbose? $verbose"


if [ "$verbose" == "true" ]
then
    readdate "$2"
else
    readdate "$1"
fi
# wenn wir noch mehr Optionen zulassen, wird das hier richtig böse!

parsedate

valid=$(./datevalidation.sh "$year" "$month" "$day" "$verbose")

if [ "$verbose" == "true" ]
then
echo "datevalidation.sh: valid date "$datum"? $valid"
fi


if [ "$valid" == "false" ]
then
    exit 1
else
    exit 0
fi
