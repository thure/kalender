#!/bin/bash

. ./globalroutines.sh

datum1="2008-09-25"

# echo "$(year $datum1)"
# echo "$(month $datum1)"
# echo "$(day $datum1)"

datum2="1997-08-31"

if isEarlierThan "$datum1" "$datum2"
then
    echo "$datum1 liegt vor $datum2"
else
    echo "$datum1 liegt nicht vor $datum2"
fi
