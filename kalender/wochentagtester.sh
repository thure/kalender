#!/bin/bash

. ./globalroutines.sh

# Test der Wochentagsberechnung

for y in $(seq 1875 2125); do ./wochentag_einfach.sh "$y" 3 8; done > einfach.txt
for y in $(seq 1875 2125); do ./wochentag_zeller.sh "$y" 3 8; done > zeller.txt
for y in $(seq 1875 2125); do date +%A --date="${y}-03-08"; done > gnu.txt

# diff -qs zeller.txt gnu.txt
diff -qs einfach.txt gnu.txt
