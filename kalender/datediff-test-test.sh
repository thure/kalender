#!/bin/bash

while read line
do

    # echo "$line"

    # continue

    date1=$(echo "$line" | awk '{print $1}')
    date2=$(echo "$line" | awk '{print $2}')

    echo ./datediff.sh "$date1" "$date2"

    ./datediff.sh "$date1" "$date2"
    echo
    echo

done < <(./datediff-test.sh 2>/dev/null | grep Fehler) 2>/dev/null


# warum immer genau ein tag abweichung, sobald ein schaltjahr beteiligt ist?


# for line in $(./datediff-test.sh 2>/dev/null | grep Fehler); do date1=$(echo "$line" | awk '{print $1}'); date2=$(echo "$line" | awk '{print $2}'); ./datediff.sh "$date1" "$date2"; echo; echo; done 2>/dev/null
