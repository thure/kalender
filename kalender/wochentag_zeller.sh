#!/bin/bash

. ./globalroutines.sh

# Wochentagsberechnung
# https://de.wikipedia.org/w/index.php?title=Zellers_Kongruenz&oldid=137649851

rounddown () {

    # Eingabe: a\in\R, a>0
    # Ausgabe: a auf zwei Stellen gerundet

   if [ 1 -ne "$#" ]
then
    echo "needs exactly one argument"
    exit 1
fi
 
   a="$1"
   r=$((100*a))
   r=$((r+r))

}

# Eingabe: Ein Datum
# Ausgabe: Der Wochentag, auf den dieses Datum fällt

if [ 3 -ne "$#" ]
then
    echo "needs year, month, day"
    exit 1
fi

# set -x

year="$1"
month="$2"
day="$3"

q=$day
if [ "$month" -lt 3 ]
then
    m=$((month+12))
    zelleryear=$((year-1))
else
    m=$month
    zelleryear=$year
fi

K=$((zelleryear%100))
J=$((zelleryear/100))
# DAMN!!!!!

# echo zelleryear=$zelleryear
# echo m=$m
# echo q=$q
# echo K=$K
# echo J=$J


W=0

W=$((W+261*m))
W=$((W-20))

W=$((W+100*K))
W=$((10000+W-((100*K)%100))) # abrunden

W=$((W+(25*K)))
W=$((10000+W-((25*K)%100))) # abrunden

W=$((W+(25*J)))
W=$((10000+W-((25*J)%100))) # abrunden

W=$((W+500*J))
W=$((W+100*q))

##########
W=$((W/2))        
W=$((W % 7))

# W=$((W + 2)) # debug



wochentage=(Samstag Sonntag Montag Dienstag Mittwoch Donnerstag Freitag)

weekdays=(Saturday Sunday Monday Tuesday Wednesday Thursday Friday)

echo "${weekdays[$W]}"

# echo debug: "${weekdays[0]}"
