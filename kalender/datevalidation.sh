#!/bin/bash

# datevalidation ist fertig auf ISO-8601-Strings umgebaut

. ./globalroutines.sh

if [ 2 -ne "$#" ]
then
    echo "needs a date string and a verbosity flag"
    exit 1
fi

year="$(year $1)"
month="$(month $1)"
day="$(day $1)"
verbose="$2"

# echo "$year"
# echo "$month"
# echo "$day"

# exit 1

if [ "$year" -lt 1600 ]
    then
        verboseoutput "Year must be greater than 1599"
        echo "false"
        exit 3
    fi

    leapyear=false
    remfour=$(($year % 4))
    remhundred=$(($year % 100))
    remfourhundred=$(($year % 400))

    # Schaltjahr-Test

    if [ "$remfour" -eq "0" ]
    then
        leapyear=true
        if [ "$remhundred" -eq "0" ]
        then
            leapyear=false
            if [ "$remfourhundred" -eq "0" ]
            then
                leapyear=true
            fi
        fi
    fi

    if [ "$leapyear" == "true" ]
    then
        verboseoutput "leap year"
    else
        verboseoutput "no leap year"
    fi


    if [ "$month" -lt 1 ]
    then
        verboseoutput "Month too small"
        echo "false"
        exit 3
    fi

    if [ "$month" -gt 12 ]
    then
        verboseoutput "Month too large"
        echo "false"
        exit 3
    fi

    if [ "$day" -lt 1 ]
    then
        verboseoutput "Day too small"
        echo "false"
        exit 3
    fi

    daysinmonth=${monthlengths[$((month))]}

    if [ "$leapyear" == "true" -a "$month" -eq 2 ]
    then
        ((daysinmonth++))
    fi

    if [ "$leapyear" == "false" -a "$month" -eq 2 ]
    then
        if [ "$day" -gt "28" ]
        then
            verboseoutput "Day too large, not a leap year"
            echo "false"
            exit 4
        fi
    fi

    if [ ${day} -gt $daysinmonth ] ; then

        verboseoutput "Day ${day} too large for month ${month}"
        verboseoutput "month ${month} has only ${daysinmonth} days"
        echo "false"
        exit 3

    fi

if [ "$leapyear" == "false" ]
then
    echo "noleap"
else
    echo "leap"

fi
