#!/bin/bash

. ./globalroutines.sh

# Wochentagsberechnung
# https://de.wikipedia.org/w/index.php?title=Wochentagsberechnung&oldid=151899358
# einige Modulo-7-Reduktionen weggelassen

# Eingabe: Ein validiertes Datum
# Ausgabe: Der Wochentag, auf den dieses Datum fällt

if [ 3 -ne "$#" ]
then
    echo "needs year, month, day"
    exit 1
fi

year="$1"
month="$2"
day="$3"

T="$day"
M="$month"
Jh=$((year/100))
Jz=$((year%100))

NT=$T # keine mod-7-Reduktion nötig

NMA=(42  0    3   3    6    1    4    6    2    5    0    3    5)
#       Jan  Feb Mrz  Apr  Mai  Jun  Jul  Aug  Sep  Okt  Nov  Dez
# 42 ist ein Platzhalter für nichtexistenten nullten Monat

NJz=$(( (Jz+(Jz/4)))) # keine mod-7-Reduktion nötig

NJh=$((  (3-(Jh % 4)) * 2 ))

if [ "$M" -lt 3 ]
then
    NSj=-1
else
    NSj=0
fi

NM=${NMA[$M]}

W=$(((NT+NM+NJh+NJz+NSj) % 7))

wochentage=(Sonntag Montag Dienstag Mittwoch Donnerstag Freitag Samstag)

weekdays=(Sunday Monday Tuesday Wednesday Thursday Friday Saturday)

echo "${weekdays[$W]}"

