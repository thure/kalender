#!/bin/bash

. ./globalroutines.sh

A=-2210418000 # 15.12.1899, seconds since epoch
B=4103694000  # 15.01.2100, seconds since epoch

howmany=1200

failures=0

for ((i=0; i<howmany; i++)); do

    failures=$((failures-1))

    firstdate=$(date +%F --date='@'"$(python -c "import random as R; print(R.randint($A, $B))")")
    seconddate=$(date +%F --date='@'"$(python -c "import random as R; print(R.randint($A, $B))")")

    if ! isLeapYear "$(year "$firstdate")" && ! isLeapYear "$(year "$seconddate")" ; then
        continue
    fi

    # if isLeapYear "$(year "$firstdate")" || isLeapYear "$(year "$seconddate")" ; then
    #     ((failures++)) # damit in der Bilanz 0 herauskommt
    #     ((i--))
    #     continue
    # fi


    debugoutput "$i"'    '"$firstdate"' '"$seconddate"

    lines=$(./datediff.sh "$firstdate" "$seconddate" 2>/dev/null | uniq | wc -l)

    if [ "$lines" -ne 1 ]
    then
        echo "$firstdate" "$seconddate"" produziert Fehler"
    fi

    failures=$((failures+lines)) # damit in der Bilanz 0 herauskommt, bei fehlerfreier Ausfuehrung

done

echo "$failures"
