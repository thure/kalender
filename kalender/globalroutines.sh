#!/bin/bash

debugoutput () {

    if [ 1 -ne "$#" ]
    then
        echo "debugoutput needs exactly one string to display"
        exit 1
    fi


    echo "DEBUG: $1" >&2

}

verboseoutput () {

    if [ "$verbose" == "true" ]
    then
        debugoutput "$1"
    fi

}

readonly monthlengths=(42 31 28 31 30 31 30 31 31 30 31 30 31)
# 42 ist Platzhalter für nichtexistenten nullten Monat



## Christian Schoens Ansatz der Datenstruktur für Kalenderdaten

# Jedes Kalenderdatum wird durch ein Array dargestellt, dessen drei
# Komponenten den Tag bzw. den Monat bzw. das Jahr festhalten.
# Für den 26. Mai 2016 ergibt sich so
# datum[0] = 2016
# datum[1] = 5
# datum[2] = 26
# Um dies hirnfreundlicher zu gestalten, definieren wir
# jahr=0, monat=1, tag=2
# und können dann beispielsweise schreiben
# ${datum[$jahr]}=$((${datum[$jahr]}+1))
# um zum nächsten Jahr überzugehen.


## Thure Dührsens Ansatz der Datenstruktur für Kalenderdaten
# Jedes Kalenderdatum wird durch einen String der Form
# "2008-06-25" dargestellt.
# Es lassen sich Arrays von Strings sehr einfach handhaben,
# im Gegensatz zu Arrays von Arrays von ganzen Zahlen.

# Getter-Funktionen

day () {

    if [ "$#" -ne "1" ]
    then
        echo "Needs exactly one argument: a string of the form 2008-06-25"
        exit 2
    fi

    echo "$((10#${1:8:2}))"

}

month () {

    if [ "$#" -ne "1" ]
    then
        echo "Needs exactly one argument: a string of the form 2008-06-25"
        exit 2
    fi

    echo "$((10#${1:5:2}))"

}

year () {

    if [ "$#" -ne "1" ]
    then
        echo "Needs exactly one argument: a string of the form 2008-06-25"
        exit 2
    fi

    echo "$((10#${1:0:4}))"

}

# Sortierfunktionen

sortdates() {

    if [ "$#" -ne 2 ]
    then
        echo "needs date1, date2"
        exit 2
    fi

    if [[ "$1" > "$2" ]]
    then
        echo "-1"
    elif [ "$1" == "$2" ]
    then
        echo "0"
    else
        echo "1"
    fi
}

isEarlierThan() {

    if [ "$#" -ne 2 ]
    then
        echo "needs date1, date2"
        exit 2
    fi

    if [ $(sortdates "$1" "$2") -eq "1" ]
    then
        return 0
    else
        return 1
    fi

}

isLeapYear () {

    if [ "$#" -ne 1 ]
    then
        echo "needs year"
        exit 2
    fi

    year="$1"
    
    local leapyear=false
    local remfour=$(($year % 4))
    local remhundred=$(($year % 100))
    local remfourhundred=$(($year % 400))

    if [ "$remfour" -eq "0" ]
    then
        leapyear=true
        if [ "$remhundred" -eq "0" ]
        then
            leapyear=false
            if [ "$remfourhundred" -eq "0" ]
            then
                leapyear=true
            fi
        fi
    fi

    if [ "$leapyear" == 'true' ]
    then
        return 0
    else
        return 1
    fi

}
