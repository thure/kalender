# README #

### What is this repository for?

*   **Quick summary**

    Einfache Kalenderrechnungen. Standardaufgaben wie Bestimmung des Wochentags zu einem Datum,
    Anzahl der Tage zwischen zwei Daten, Bestimmung des Datums, auf das ein bestimmter Tag eines
    Zyklus vorgegebener Länge fällt....

    Berechnungen von Mondphasen etc. (Zyklen mit Schwankungen....)

    evtl. Modularisierung für die Erweiterbarkeit auf weniger gebräuchliche bis exotische Kalender.

    **Nichttriviale Algorithmen können (gern in Gedichtform) erläutert
      werden.**
    

*   **Version**  
    0.1 alpha
    

### How do I get set up?

*   **Summary of set up**

        #!/bin/bash
        
        cd /path/to/awesome/projects/
        
        git clone git@bitbucket.org:thure/biginteger.git
        cd kalender
            

*   **Configuration**  
    Quasi nicht existent. Noch ist das Projekt klein genug.
    

*   **Dependencies**  
    Die Build-Umgebung ist Unix-lastig.  
    [`bash`](https://packages.debian.org/stable/bash
      "Bourne Again Shell") reicht im Wesentlichen aus.
    

*   **How to run tests**  
    `cd tests`
    

*   **Deployment instructions**  
    So weit sind wir noch nicht :)  

    


### Contribution guidelines



### Who do I talk to?

Thure Dührsen  
`T.Duehrsen@gmx.net`

Christian Schoen  
`csc@sags-per-mail.de`